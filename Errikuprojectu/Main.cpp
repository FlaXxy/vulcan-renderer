#include <vector>

#include "volk.h"

#define SAFE(x) if((x)!=VkResult::VK_SUCCESS) return -1;

static VKAPI_ATTR VkBool32 VKAPI_CALL myCallback(VkDebugReportFlagsEXT flags, VkDebugReportObjectTypeEXT objType,
	uint64_t obj,
	size_t location,
	int32_t code,
	const char* layerPrefix,
	const char* msg,
	void* userData)
{
	return VK_FALSE;
}

int main()
{
	auto res = volkInitialize();
	if (res != VK_SUCCESS)
	{
		return -1;
	}
	std::vector<const char*> extensions{VK_EXT_DEBUG_REPORT_EXTENSION_NAME};

	VkInstanceCreateInfo info{
		VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
		nullptr,
		0,
		nullptr,
		0,
		nullptr,
		extensions.size(),
		extensions.data()
	};
	VkInstance instance;
	SAFE(vkCreateInstance(&info, nullptr, &instance));
	volkLoadInstance(instance);

	VkDebugReportCallbackCreateInfoEXT callbackCi{
		VK_STRUCTURE_TYPE_DEBUG_REPORT_CALLBACK_CREATE_INFO_EXT,
		nullptr,
		VK_DEBUG_REPORT_WARNING_BIT_EXT|VK_DEBUG_REPORT_ERROR_BIT_EXT,
		myCallback,
		nullptr
	};
	VkDebugReportCallbackEXT callback;
	SAFE(vkCreateDebugReportCallbackEXT(instance,&callbackCi,nullptr,&callback));
	return 0;
}
