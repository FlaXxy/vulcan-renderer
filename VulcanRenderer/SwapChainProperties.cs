﻿using Vulkan;

namespace VulcanRenderer
{
    public record SwapChainProperties(VkSurfaceCapabilitiesKHR Capabilities, VkSurfaceFormatKHR[] Formats,
        VkPresentModeKHR[] PresentModes)
    {
    }
}