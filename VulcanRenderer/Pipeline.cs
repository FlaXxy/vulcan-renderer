﻿using Vulkan;

namespace VulcanRenderer
{
    internal record Pipeline(VkPipeline VkPipeline, VkRenderPass Pass, VkPipelineLayout Layout)
    {
        public unsafe void Destroy(VkDevice device)
        {
            VulkanNative.vkDestroyPipeline(device, VkPipeline, null);
            VulkanNative.vkDestroyPipelineLayout(device, Layout, null);
            VulkanNative.vkDestroyRenderPass(device, Pass, null);
        }
    }
}