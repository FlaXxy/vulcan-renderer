﻿using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Runtime.InteropServices;
using SDL2;
using Vulkan;
using static Vulkan.VulkanNative;

namespace VulcanRenderer
{
    internal unsafe class Program
    {
        private static readonly PFN_vkDebugReportCallbackEXT debugCallbackDelegate = debugCallback;

        private static readonly VkComponentMapping IdentityMapping = new()
        {
            a = VkComponentSwizzle.Identity,
            r = VkComponentSwizzle.Identity,
            g = VkComponentSwizzle.Identity,
            b = VkComponentSwizzle.Identity
        };

        private static readonly Assembly assembly = typeof(Program).Assembly;

        private static void Main(string[] args)
        {
            InitSDL();
            var window = CreateWindow();
            var instance = CreateInstance(window);
            var debugExtension = SetupDebugCallbacks(instance);
            var physicalDevice = SelectPhysicalDevice(instance);
            var surface = CreateSurface(instance, window);
            var qindex = SelectQueueFamilyIndex(physicalDevice, surface) ??
                         throw new Exception("Required Queue not found");
            var device = SelectDevice(physicalDevice, qindex);
            var queue = GetQueue(device, qindex);
            var swapchain = CreateSwapchain(physicalDevice, device, surface, window);
            var pipeline = CreatePipeline(device, swapchain);
            var framebuffers = CreateFramebuffers(device, swapchain, pipeline);
            var commandPool = CreateCommandPool(device, qindex);

            var commandBuffers = CreateCommandBuffers(device, commandPool, framebuffers, pipeline, swapchain);

            var imageAvailableSemaphore = CreateSemaphore(device);
            var renderFinishedSemaphore = CreateSemaphore(device);

            {
                var run = true;
                uint imageIndex = 0;
                while (run)
                {
                    while (SDL.SDL_PollEvent(out var evt) != 0)
                        if (evt.type == SDL.SDL_EventType.SDL_QUIT)
                            run = false;
                    CheckResult(vkAcquireNextImageKHR(device, swapchain.Swapchain, ulong.MaxValue, imageAvailableSemaphore, VkFence.Null, ref imageIndex));
                    Submit(commandBuffers[imageIndex], queue, imageAvailableSemaphore, renderFinishedSemaphore);
                    Present(queue, imageIndex, swapchain, renderFinishedSemaphore);

                    //the tutorial says, it works without this, but otherwise vulkan validation throws warnings.
                    //the tutorial implements synchronization with multiple imageAvailable/renderFinished Semaphores, but this is still a todo.
                    //right now I am introducing an unwanted synchronization point here, where the CPU could work while the GPU is working.
                    vkQueueWaitIdle(queue);
                }
            }

            vkDestroySemaphore(device, imageAvailableSemaphore, null);
            vkDestroySemaphore(device, renderFinishedSemaphore, null);
            vkDestroyCommandPool(device, commandPool, null);
            foreach (var framebuffer in framebuffers) vkDestroyFramebuffer(device, framebuffer, null);

            pipeline.Destroy(device);
            swapchain.Destroy(device);
            vkDestroyDevice(device, null);
            vkDestroySurfaceKHR(instance, surface, null);
            var vkDestroyDebugReportCallbackEXT =
                GetInstanceCallback<vkDestroyDebugReportCallbackEXT_d>(instance,
                    nameof(VulkanNative.vkDestroyDebugReportCallbackEXT));
            vkDestroyDebugReportCallbackEXT(instance, debugExtension, null);
            vkDestroyInstance(instance, null);
            SDL.SDL_DestroyWindow(window);
        }

        private static void Present(VkQueue queue, uint imageIndex, SwapChain swapchain, VkSemaphore renderReady)
        {
            var swapChain = swapchain.Swapchain;
            var presentInfo = VkPresentInfoKHR.New();
            presentInfo.swapchainCount = 1;
            presentInfo.pImageIndices = &imageIndex;
            presentInfo.pSwapchains = &swapChain;
            presentInfo.pResults = null;
            presentInfo.waitSemaphoreCount = 1;
            presentInfo.pWaitSemaphores = &renderReady;
            CheckResult(vkQueuePresentKHR(queue, ref presentInfo));
        }

        private static void Submit(VkCommandBuffer commandBuffer, VkQueue queue, VkSemaphore imageAvailableSemaphore, VkSemaphore renderFinishedSemaphore)
        {
            var waitStages = VkPipelineStageFlags.ColorAttachmentOutput;
            var submitInfo = VkSubmitInfo.New();
            submitInfo.commandBufferCount = 1;
            submitInfo.pCommandBuffers = &commandBuffer;

            submitInfo.waitSemaphoreCount = 1;
            submitInfo.pWaitSemaphores = &imageAvailableSemaphore;
            submitInfo.pWaitDstStageMask = &waitStages;

            submitInfo.signalSemaphoreCount = 1;
            submitInfo.pSignalSemaphores = &renderFinishedSemaphore;

            CheckResult(vkQueueSubmit(queue, 1, ref submitInfo, VkFence.Null));
        }

        private static VkSemaphore CreateSemaphore(VkDevice device)
        {
            var ci = VkSemaphoreCreateInfo.New();
            CheckResult(vkCreateSemaphore(device, ref ci, null, out var semaphore));
            return semaphore;
        }

        private static VkCommandBuffer[] CreateCommandBuffers(VkDevice device, VkCommandPool commandPool, VkFramebuffer[] framebuffers, Pipeline pipeline, SwapChain swapChain)
        {
            var allocInfo = VkCommandBufferAllocateInfo.New();
            allocInfo.commandPool = commandPool;
            allocInfo.level = VkCommandBufferLevel.Primary;
            allocInfo.commandBufferCount = (uint) framebuffers.Length;
            var commandBuffers = new VkCommandBuffer[framebuffers.Length];

            CheckResult(vkAllocateCommandBuffers(device, ref allocInfo, out commandBuffers[0]));


            var clearColor = new VkClearValue
            {
                color = new VkClearColorValue(1f, 0, 1f)
            };
            //record them here, normally, you need to record them every frame and then play them.
            foreach (var (commandBuffer, framebuffer) in commandBuffers.Zip(framebuffers))
            {
                var commandBufferBeginInfo = VkCommandBufferBeginInfo.New();
                commandBufferBeginInfo.flags = VkCommandBufferUsageFlags.None;
                commandBufferBeginInfo.pInheritanceInfo = null;
                CheckResult(vkBeginCommandBuffer(commandBuffer, ref commandBufferBeginInfo));

                var beginInfo = VkRenderPassBeginInfo.New();
                beginInfo.renderPass = pipeline.Pass;
                beginInfo.framebuffer = framebuffer;
                beginInfo.renderArea.offset = VkOffset2D.Zero;
                beginInfo.renderArea.extent = swapChain.ImagExtent;
                beginInfo.clearValueCount = 1;
                beginInfo.pClearValues = &clearColor;
                vkCmdBeginRenderPass(commandBuffer, ref beginInfo, VkSubpassContents.Inline);
                vkCmdBindPipeline(commandBuffer, VkPipelineBindPoint.Graphics, pipeline.VkPipeline);
                vkCmdDraw(commandBuffer, 3, 1, 0, 0);
                vkCmdEndRenderPass(commandBuffer);

                CheckResult(vkEndCommandBuffer(commandBuffer));
            }

            return commandBuffers;
        }

        private static VkCommandPool CreateCommandPool(VkDevice device, uint qindex)
        {
            var ci = VkCommandPoolCreateInfo.New();
            ci.queueFamilyIndex = qindex;
            ci.flags = VkCommandPoolCreateFlags.None;
            CheckResult(vkCreateCommandPool(device, ref ci, null, out var commandPool));
            return commandPool;
        }

        private static VkFramebuffer[] CreateFramebuffers(VkDevice device, SwapChain swapchain, Pipeline pipeline)
        {
            return swapchain.ImageViews.Select(imageView =>
            {
                var ci = VkFramebufferCreateInfo.New();
                ci.renderPass = pipeline.Pass;
                ci.attachmentCount = 1;
                ci.pAttachments = &imageView;
                ci.width = swapchain.ImagExtent.width;
                ci.height = swapchain.ImagExtent.height;
                ci.layers = 1;
                CheckResult(vkCreateFramebuffer(device, ref ci, null, out var framebuffer));
                return framebuffer;
            }).ToArray();
        }

        private static Pipeline CreatePipeline(VkDevice device, SwapChain swapChain)
        {
            var vertexShader = CreateShaderModule(device, "shader:/shader.vert.spv");
            var fragmentShader = CreateShaderModule(device, "shader:/shader.frag.spv");

            var vertexShaderCi = VkPipelineShaderStageCreateInfo.New();
            vertexShaderCi.stage = VkShaderStageFlags.Vertex;
            vertexShaderCi.module = vertexShader;
            vertexShaderCi.pName = Strings.main;

            var fragmentShaderCi = VkPipelineShaderStageCreateInfo.New();
            fragmentShaderCi.stage = VkShaderStageFlags.Fragment;
            fragmentShaderCi.module = fragmentShader;
            fragmentShaderCi.pName = Strings.main;

            var inputStateCi = VkPipelineVertexInputStateCreateInfo.New();
            inputStateCi.vertexBindingDescriptionCount = 0;
            inputStateCi.vertexAttributeDescriptionCount = 0;

            var inputAssemblyCi = VkPipelineInputAssemblyStateCreateInfo.New();
            inputAssemblyCi.topology = VkPrimitiveTopology.TriangleList;
            inputAssemblyCi.primitiveRestartEnable = false;

            var viewport = new VkViewport
            {
                x = 0,
                y = 0,
                height = swapChain.ImagExtent.height,
                width = swapChain.ImagExtent.width,
                minDepth = 0,
                maxDepth = 1
            };

            var scissor = new VkRect2D(swapChain.ImagExtent);

            var viewPortCi = VkPipelineViewportStateCreateInfo.New();
            viewPortCi.scissorCount = 1;
            viewPortCi.pScissors = &scissor;
            viewPortCi.viewportCount = 1;
            viewPortCi.pViewports = &viewport;

            var rasterizerCi = VkPipelineRasterizationStateCreateInfo.New();
            rasterizerCi.depthClampEnable = false;
            rasterizerCi.rasterizerDiscardEnable = false;
            rasterizerCi.polygonMode = VkPolygonMode.Fill;
            rasterizerCi.lineWidth = 1;
            rasterizerCi.cullMode = VkCullModeFlags.Back;
            rasterizerCi.frontFace = VkFrontFace.CounterClockwise;
            rasterizerCi.depthBiasEnable = false;

            var multisampleCi = VkPipelineMultisampleStateCreateInfo.New();
            multisampleCi.sampleShadingEnable = false;
            multisampleCi.rasterizationSamples = VkSampleCountFlags.Count1;
            multisampleCi.minSampleShading = 1;
            multisampleCi.pSampleMask = null;
            multisampleCi.alphaToCoverageEnable = false;
            multisampleCi.alphaToOneEnable = false;

            var colorBlendAttachmentState = new VkPipelineColorBlendAttachmentState
            {
                colorWriteMask = VkColorComponentFlags.A | VkColorComponentFlags.B | VkColorComponentFlags.G |
                                 VkColorComponentFlags.R,
                blendEnable = false
            };

            var colorBlendCi = VkPipelineColorBlendStateCreateInfo.New();
            colorBlendCi.logicOpEnable = false;
            colorBlendCi.attachmentCount = 1;
            colorBlendCi.pAttachments = &colorBlendAttachmentState;

            var pipelineLayoutCi = VkPipelineLayoutCreateInfo.New();
            CheckResult(vkCreatePipelineLayout(device, ref pipelineLayoutCi, null, out var pipelineLayout));

            var colorAttachment = new VkAttachmentDescription
            {
                format = swapChain.ImageFormat,
                samples = VkSampleCountFlags.Count1,
                loadOp = VkAttachmentLoadOp.Clear,
                storeOp = VkAttachmentStoreOp.Store,
                stencilLoadOp = VkAttachmentLoadOp.DontCare,
                stencilStoreOp = VkAttachmentStoreOp.DontCare,
                initialLayout = VkImageLayout.Undefined,
                finalLayout = VkImageLayout.PresentSrcKHR
            };

            var colorAttachmentRef = new VkAttachmentReference
            {
                attachment = 0,
                layout = VkImageLayout.ColorAttachmentOptimal
            };

            var subpass = new VkSubpassDescription
            {
                colorAttachmentCount = 1,
                pColorAttachments = &colorAttachmentRef,
                pipelineBindPoint = VkPipelineBindPoint.Graphics
            };


            var subpassDependency = new VkSubpassDependency
            {
                dstSubpass = 0,
                dstStageMask = VkPipelineStageFlags.ColorAttachmentOutput,
                dstAccessMask = VkAccessFlags.ColorAttachmentWrite,
                srcSubpass = RawConstants.VK_SUBPASS_EXTERNAL,
                srcStageMask = VkPipelineStageFlags.ColorAttachmentOutput,
                srcAccessMask = VkAccessFlags.None
            };

            var renderPassCi = VkRenderPassCreateInfo.New();
            renderPassCi.attachmentCount = 1;
            renderPassCi.pAttachments = &colorAttachment;
            renderPassCi.subpassCount = 1;
            renderPassCi.pSubpasses = &subpass;
            renderPassCi.dependencyCount = 1;
            renderPassCi.pDependencies = &subpassDependency;

            CheckResult(vkCreateRenderPass(device, ref renderPassCi, null, out var renderPass));
            var stages = stackalloc VkPipelineShaderStageCreateInfo[2] {vertexShaderCi, fragmentShaderCi};

            var pipelineCi = VkGraphicsPipelineCreateInfo.New();
            pipelineCi.renderPass = renderPass;
            pipelineCi.stageCount = 2;
            pipelineCi.pStages = stages;
            pipelineCi.pVertexInputState = &inputStateCi;
            pipelineCi.pInputAssemblyState = &inputAssemblyCi;
            pipelineCi.pViewportState = &viewPortCi;
            pipelineCi.pRasterizationState = &rasterizerCi;
            pipelineCi.pMultisampleState = &multisampleCi;
            pipelineCi.pDepthStencilState = null;
            pipelineCi.pColorBlendState = &colorBlendCi;
            pipelineCi.pDynamicState = null;
            pipelineCi.layout = pipelineLayout;
            pipelineCi.renderPass = renderPass;
            pipelineCi.subpass = 0;
            pipelineCi.basePipelineHandle = VkPipeline.Null;
            pipelineCi.basePipelineIndex = -1;

            CheckResult(vkCreateGraphicsPipelines(device, VkPipelineCache.Null, 1, ref pipelineCi, null,
                out var pipeline));

            vkDestroyShaderModule(device, vertexShader, null);
            vkDestroyShaderModule(device, fragmentShader, null);
            return new Pipeline(pipeline, renderPass, pipelineLayout);
        }


        private static VkShaderModule CreateShaderModule(VkDevice device, string shaderPath)
        {
            var names = assembly.GetManifestResourceNames();
            using var stream = assembly.GetManifestResourceStream(shaderPath) ??
                               throw new Exception("No Resource for Shader found");
            var buffer = new byte[stream.Length];
            using var mem = new MemoryStream(buffer);
            stream.CopyTo(mem);

            var ci = VkShaderModuleCreateInfo.New();
            ci.codeSize = (UIntPtr) buffer.Length;
            fixed (byte* ptr = buffer)
            {
                ci.pCode = (uint*) ptr;
                CheckResult(vkCreateShaderModule(device, &ci, null, out var res));
                return res;
            }
        }

        private static VkImage[] GetSwapImages(VkDevice device, VkSwapchainKHR swapchain)
        {
            uint count = 0;
            vkGetSwapchainImagesKHR(device, swapchain, ref count, null);
            var res = new VkImage[count];
            vkGetSwapchainImagesKHR(device, swapchain, ref count, out res[0]);
            return res;
        }

        private static SwapChain CreateSwapchain(VkPhysicalDevice physicalDevice, VkDevice device, VkSurfaceKHR surface, IntPtr window)
        {
            var swapChainProps = GetSwapChainProps(physicalDevice, surface);
            var surfaceFormat = GetSurfaceFormat(swapChainProps);
            var presentMode = GetPresentMode(swapChainProps);
            var swapExtent = GetSwapExtent(swapChainProps, window);
            var imageCount = GetImageCount(swapChainProps);

            var ci = VkSwapchainCreateInfoKHR.New();
            ci.surface = surface;
            ci.presentMode = presentMode;
            ci.minImageCount = imageCount;
            ci.imageColorSpace = surfaceFormat.colorSpace;
            ci.imageFormat = surfaceFormat.format;
            ci.imageArrayLayers = 1;
            ci.imageUsage = VkImageUsageFlags.ColorAttachment;
            ci.imageExtent = swapExtent;
            //when using multiple queues, specify them here and specify the Sharingmode.
            ci.imageSharingMode = VkSharingMode.Exclusive;
            ci.preTransform = swapChainProps.Capabilities.currentTransform;
            ci.compositeAlpha = VkCompositeAlphaFlagsKHR.OpaqueKHR;
            ci.clipped = true;
            ci.oldSwapchain = VkSwapchainKHR.Null;
            CheckResult(vkCreateSwapchainKHR(device, ref ci, null, out var swapchain));

            var swapImages = GetSwapImages(device, swapchain);
            var views = GetImageViews(device, swapImages, surfaceFormat.format);

            return new SwapChain(swapchain, swapImages, views, swapExtent, surfaceFormat.format);
        }

        private static VkImageView[] GetImageViews(VkDevice device, VkImage[] swapImages, VkFormat format)
        {
            return swapImages.Select(image =>
            {
                var ci = VkImageViewCreateInfo.New();
                ci.image = image;
                ci.viewType = VkImageViewType.Image2D;
                ci.format = format;
                ci.components = IdentityMapping;
                ci.subresourceRange.aspectMask = VkImageAspectFlags.Color;
                ci.subresourceRange.baseMipLevel = 0;
                ci.subresourceRange.levelCount = 1;
                ci.subresourceRange.baseArrayLayer = 0;
                ci.subresourceRange.layerCount = 1;
                CheckResult(vkCreateImageView(device, ref ci, null, out var view));
                return view;
            }).ToArray();
        }

        private static uint GetImageCount(SwapChainProperties swapChainProps)
        {
            var count = swapChainProps.Capabilities.minImageCount + 1;
            var maxImageCount = swapChainProps.Capabilities.maxImageCount;
            if (maxImageCount > 0 && count > maxImageCount)
                count = maxImageCount;
            return count;
        }


        private static VkExtent2D GetSwapExtent(SwapChainProperties swapChainProps, IntPtr window)
        {
            var capabilities = swapChainProps.Capabilities;
            if (capabilities.currentExtent.width != uint.MaxValue) return capabilities.currentExtent;

            SDL.SDL_GL_GetDrawableSize(window, out var w, out var h);
            Math.Clamp(w, capabilities.minImageExtent.width, capabilities.maxImageExtent.width);
            Math.Clamp(h, capabilities.minImageExtent.height, capabilities.maxImageExtent.height);
            return new VkExtent2D(w, h);
        }

        private static VkPresentModeKHR GetPresentMode(SwapChainProperties swapChainProps)
        {
            if (swapChainProps.PresentModes.Contains(VkPresentModeKHR.ImmediateKHR))
                return VkPresentModeKHR.ImmediateKHR;
            if (swapChainProps.PresentModes.Contains(VkPresentModeKHR.MailboxKHR))
                return VkPresentModeKHR.MailboxKHR;

            return VkPresentModeKHR.FifoKHR;
        }

        private static VkSurfaceFormatKHR GetSurfaceFormat(SwapChainProperties swapChainProps)
        {
            foreach (var format in swapChainProps.Formats)
                if (format.colorSpace == VkColorSpaceKHR.SrgbNonlinearKHR && format.format == VkFormat.B8g8r8a8Srgb)
                    return format;

            throw new Exception("No fitting surface format found");
        }

        private static SwapChainProperties GetSwapChainProps(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface)
        {
            CheckResult(vkGetPhysicalDeviceSurfaceCapabilitiesKHR(physicalDevice, surface, out var capabilities));
            uint count = 0;

            CheckResult(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, ref count, null));

            var formats = new VkSurfaceFormatKHR[count];
            CheckResult(vkGetPhysicalDeviceSurfaceFormatsKHR(physicalDevice, surface, ref count, out formats[0]));

            count = 0;
            CheckResult(vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, ref count, null));

            var presentModes = new VkPresentModeKHR[count];
            CheckResult(
                vkGetPhysicalDeviceSurfacePresentModesKHR(physicalDevice, surface, ref count, out presentModes[0]));

            return new SwapChainProperties(capabilities, formats, presentModes);
        }

        private static VkQueue GetQueue(VkDevice device, uint qindex)
        {
            vkGetDeviceQueue(device, qindex, 0, out var result);
            return result;
        }

        private static VkSurfaceKHR CreateSurface(VkInstance instance, IntPtr window)
        {
            CheckResult(SDL.SDL_Vulkan_CreateSurface(window, instance.Handle, out var surface));
            return new VkSurfaceKHR(surface);
        }

        private static uint? SelectQueueFamilyIndex(VkPhysicalDevice physicalDevice, VkSurfaceKHR surface)
        {
            uint count = 0;
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, ref count, null);
            var families = new VkQueueFamilyProperties[count];
            vkGetPhysicalDeviceQueueFamilyProperties(physicalDevice, ref count, out families[0]);
            uint i = 0;
            foreach (var vkQueueFamilyProperty in families)
            {
                if (vkQueueFamilyProperty.queueFlags.HasFlag(VkQueueFlags.Graphics))
                {
                    if (!vkQueueFamilyProperty.queueFlags.HasFlag(VkQueueFlags.Transfer))
                        Console.WriteLine("My selected Q family doesn't have the Transfer flag?!");

                    CheckResult(vkGetPhysicalDeviceSurfaceSupportKHR(physicalDevice, i, surface,
                        out var supported));
                    if (supported)
                        return i;
                }

                i++;
            }

            return null;
        }

        private static VkDevice SelectDevice(VkPhysicalDevice physicalDevice, uint qindex)
        {
            var qci = VkDeviceQueueCreateInfo.New();
            var myPriority = 1.0f;
            qci.pQueuePriorities = &myPriority;
            qci.queueCount = 1;
            qci.queueFamilyIndex = qindex;

            var ci = VkDeviceCreateInfo.New();
            ci.pQueueCreateInfos = &qci;
            ci.queueCreateInfoCount = 1;
            ci.enabledLayerCount = 0;
            ci.enabledExtensionCount = 1;

            byte* ptr = Strings.VK_KHR_SWAPCHAIN_EXTENSION_NAME;

            ci.ppEnabledExtensionNames = &ptr;
            CheckResult(vkCreateDevice(physicalDevice, ref ci, null, out var result));
            return result;
        }


        private static TDelegate GetInstanceCallback<TDelegate>(VkInstance instance, string functioName)
        {
            TDelegate result;
            using FixedUtf8String name = functioName;
            var ptr = vkGetInstanceProcAddr(instance, name);
            result =
                Marshal.GetDelegateForFunctionPointer<TDelegate>(ptr);
            return result;
        }

        private static VkDebugReportCallbackEXT SetupDebugCallbacks(VkInstance instance)
        {
            var ci = VkDebugReportCallbackCreateInfoEXT.New();
            ci.flags = VkDebugReportFlagsEXT.ErrorEXT | VkDebugReportFlagsEXT.WarningEXT;
            ci.pfnCallback = Marshal.GetFunctionPointerForDelegate(debugCallbackDelegate);
            ci.pNext = null;
            ci.pUserData = null;
            var vkCreateDebugReportCallbackEXT =
                GetInstanceCallback<vkCreateDebugReportCallbackEXT_d>(instance,
                    nameof(VulkanNative.vkCreateDebugReportCallbackEXT));
            vkCreateDebugReportCallbackEXT(instance, &ci, null, out var ext);
            return ext;
        }

        private static uint debugCallback(
            uint flags,
            VkDebugReportObjectTypeEXT objectType,
            ulong @object,
            UIntPtr location,
            int messageCode,
            byte* pLayerPrefix,
            byte* pMessage,
            void* pUserData)
        {
            var layer = Marshal.PtrToStringUTF8((IntPtr) pLayerPrefix);
            var msg = Marshal.PtrToStringUTF8((IntPtr) pMessage);
            Console.Error.WriteLine($"Vulkan({layer}): {msg}");
            return VkBool32.False;
        }

        private static IntPtr CreateWindow()
        {
            return SDL.SDL_CreateWindow("MyWindow", SDL.SDL_WINDOWPOS_CENTERED, SDL.SDL_WINDOWPOS_CENTERED, 1024, 786, SDL.SDL_WindowFlags.SDL_WINDOW_VULKAN | SDL.SDL_WindowFlags.SDL_WINDOW_SHOWN);
        }

        private static void InitSDL()
        {
            CheckResult(SDL.SDL_Init(SDL.SDL_INIT_VIDEO | SDL.SDL_INIT_EVENTS));
        }

        private static VkPhysicalDevice SelectPhysicalDevice(VkInstance instance)
        {
            uint count = 0;
            vkEnumeratePhysicalDevices(instance, ref count, null);
            var d = new VkPhysicalDevice[(int) count];
            vkEnumeratePhysicalDevices(instance, ref count, ref d[0]);
            if (count == 0)
                throw new Exception("no devices found");
            return d[0];
        }


        public static VkInstance CreateInstance(IntPtr wnd)
        {
            {
                uint propc = 0;
                vkEnumerateInstanceExtensionProperties((byte*) null, ref propc, null);
                var props = new VkExtensionProperties[(int) propc];

                vkEnumerateInstanceExtensionProperties((byte*) null, ref propc, ref props[0]);
                foreach (var vkExtensionProperties in props)
                    Console.WriteLine(
                        $"Available Extension: {Marshal.PtrToStringUTF8((IntPtr) vkExtensionProperties.extensionName)}");
            }

            SDL.SDL_Vulkan_GetInstanceExtensions(wnd, out var extCount, null);
            var requiredExtensions = new IntPtr[extCount];
            SDL.SDL_Vulkan_GetInstanceExtensions(wnd, out extCount, requiredExtensions);

            var appInfo = VkApplicationInfo.New();
            appInfo.pApplicationName = Strings.AppName;
            appInfo.pEngineName = Strings.EngineName;
            appInfo.apiVersion = new Version(1, 0, 0);
            appInfo.engineVersion = new Version(1, 0, 0);
            appInfo.apiVersion = new Version(1, 0, 0);

            var instanceLayers = new RawList<IntPtr>();

            var instanceExtensions = new RawList<IntPtr>();
            foreach (var intPtr in requiredExtensions) instanceExtensions.Add(intPtr);

            var debug = true;
            if (debug)
            {
                instanceExtensions.Add(Strings.VK_EXT_DEBUG_REPORT_EXTENSION_NAME);
                instanceLayers.Add(Strings.StandardValidationLayerName);
                foreach (var instanceExtension in instanceExtensions)
                {
                    var s = Marshal.PtrToStringUTF8(instanceExtension);
                    Console.WriteLine($"InstanceExtension {s} requested.");
                }

                foreach (var instanceLayer in instanceLayers)
                {
                    var s = Marshal.PtrToStringUTF8(instanceLayer);
                    Console.WriteLine($"InstanceLayer {s} requested.");
                }
            }


            var instanceCreateInfo = VkInstanceCreateInfo.New();
            instanceCreateInfo.pApplicationInfo = &appInfo;
            fixed (IntPtr* extensionsBase = &instanceExtensions.Items[0])
            fixed (IntPtr* layersBase = &instanceLayers.Items[0])
            {
                instanceCreateInfo.enabledExtensionCount = instanceExtensions.Count;
                instanceCreateInfo.ppEnabledExtensionNames = (byte**) extensionsBase;
                instanceCreateInfo.enabledLayerCount = instanceLayers.Count;
                instanceCreateInfo.ppEnabledLayerNames = (byte**) layersBase;
                CheckResult(vkCreateInstance(ref instanceCreateInfo, null, out var instance));
                return instance;
            }
        }

        private static void CheckResult(SDL.SDL_bool result)
        {
            if (result != SDL.SDL_bool.SDL_TRUE) throw new Exception($"SDL call was not successful: {result}");
        }

        private static void CheckResult(int result)
        {
            if (result != 0) throw new Exception($"SDL call was not successful: {result}");
        }

        private static void CheckResult(VkResult result)
        {
            if (result != VkResult.Success) throw new Exception($"Vulkan call was not successful: {result}");
        }

        internal delegate VkResult vkCreateDebugReportCallbackEXT_d(
            VkInstance instance,
            VkDebugReportCallbackCreateInfoEXT* createInfo,
            void* allocatorPtr,
            out VkDebugReportCallbackEXT ret);

        internal delegate void vkDestroyDebugReportCallbackEXT_d(
            VkInstance instance,
            VkDebugReportCallbackEXT callback,
            VkAllocationCallbacks* pAllocator);
    }
}