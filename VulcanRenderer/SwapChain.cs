﻿using Vulkan;

namespace VulcanRenderer
{
    public record SwapChain(VkSwapchainKHR Swapchain, VkImage[] Images, VkImageView[] ImageViews, VkExtent2D ImagExtent,
        VkFormat ImageFormat)
    {
        public unsafe void Destroy(VkDevice device)
        {
            foreach (var imageView in ImageViews) VulkanNative.vkDestroyImageView(device, imageView, null);
            VulkanNative.vkDestroySwapchainKHR(device, Swapchain, null);
        }
    }
}